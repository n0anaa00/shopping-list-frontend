
import './App.css';
import {useState} from 'react';
import axios from 'axios';


const URL = 'http://localhost:3000/shoppinglist/';




function App() {
  const [descriptions, setDescriptions] = useState([]);
  const [description, setDescription] = useState('');
  const [amounts, setAmounts] = useState([]);
  const [amount, setAmount] = useState('');
  
  const [items, setItems] = useState([]);
  const [editItem, setEditItem] = useState('');


  
  useEffect(() =>{
    axios.get(URL)
    .then((response) => {
      setDescriptions(response.data)
      setAmounts(response.data)
    }).catch(error => {
      alert(error.response ? error.response.data.error : error);
    })
  }, []);
 
  function save(e) {
    e.preventDefault();
    const json = JSON.stringify({description:description})
    const json2 = JSON.stringify({description:amounts})
    axios.post(URL + 'add.php', json, json2, {
      headers: {
        'Content-Type' : 'application/json'
      }
    })
    
    .then((response) => {
      setDescriptions(descriptions => [...descriptions,response.data]);
      setDescription('');
      setAmounts(amounts => [...amounts,response.data]);
      setAmount('');
    }).catch(error => {
      alert(error.response.data.error)
    });
    
    
    
  }

  function remove(id){
    const json = JSON.stringify({id:id})
    axios.post(URL + 'delete.php',json,{
      headers: {
        'Content-Type' : 'application/json'
      }
    })

    .then((response) => {
      const newListWithoutRemoved = descriptions.filter((item) => item.id !== id);
      const newListWithoutRemoved2 = amounts.filter((item) => item.id !== id);
      setTasks(newListWithoutRemoved);
      setTasks(newListWithoutRemoved2);

    })

  }

  function update(e) {
    e.preventDefault();
    const json = JSON.stringify({id:editItem.id})
    axios.post(URL + 'update.php', json, {
      headers: {
        'Content-Type' : 'application/json'
      }
    })

    .then((response) => {
      descriptions[(descriptions.findIndex(description => description.id === editItem))];
      amounts[(amounts.findIndex(amount => amount.id === editItem))];
      setItems([...descriptions]);
      setItems([...amounts]);
    })
  }

  return (
    
    <div className="container">
    
    <form onSubmit={save}>  </form>   

      <h3>Shopping List</h3>
        <div>
            <label>New Item</label>&nbsp;
            <input type="text" step="1"
            value={eur} onChange={e => setDescription(e.target.value)}/>
            
            <input type="number" step="1" 
            value={eur} onChange={e => setAmount(e.target.value)}/>
            
            
            <button>Add</button>
        </div>
        
              <ol>
               
                {items.map(description =>(
                  
                  <form onSubmit={update}>
                  
                {editItem?.id === item.id &&
                  
                  
                
                  
                    <a className="delete" onClick={() => remove(item.id)} href="#">
                      Delete
                    </a>&nbsp}

                

    </form> 

            ))}


    </ol>
          
              
         
      
      

    </div> 

    
     );

  

}

export default App;
